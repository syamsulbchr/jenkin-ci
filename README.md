# Jenkin Complete with Blue Ocean
Pada repo ini terdapat konfigurasi dari jenkin yang di jalankan dengan docker container, pada jenkin ini sudah terinstall BlueOcean plugin yang dapat di gunakan untuk melihat interface jenkin yang lebih user friendly

semua configurasi dan data yang ada di dalam jenkin akan di simpan pada folder "jenkin_data"

## Menjalankan Jenkin Dengan Docker
untuk menjalankan Aplikasi jenkin ini kita hanya perlu mengikuti step2 sebagai berikut ini :

### Clone Repo
pull repo ke local pc/laptop/server anda dengan perintah sebagai beriikut

```
git clone https://gitlab.com/devops161/jenkins.git
```
### Set Jenkin Data Volume
rubah properti file "jenkin_data" menjadi writeble
```bash
chmod -R 777 jenkins_data
```
### Running Docker Compose
jalankan compose file 
```bash
docker-compose up -d
```
pastikan anda sudah mempunya docker compose yang terinstall di local anda
perintah di atas akan menjalankan "docker-compose.yml" dengan tanpa menampilkan log process dari docker tersebut jika anda ingin menampilkan log process nya likangkan -d nya saja maka log akan di tampilkan di layar

### Check Container
setelah menjalankan perintah di atas kita bisa lihat apakah status container nya sudah berhasil di running atau belum dengan perintah berikut 
```bash
docker ps
```
### Running Jenkins
ketikan container sudah jalan maka anda bisa mengakses aplikasi jenkins dengan mengetikan http://localhost:8080 pada browser anda untuk pertama kali anda akan di minta untuk memasukan password administrator jenkin,
untuk mendapatkan password administrator yang di generate oleh jenkin anda harus masuk kedalam file dimana password tersebut di simpan.

password administrator default jenkin di simpan pada folder "/var/jenkins_home/secrets/initialAdminPassword",
dikarenakan kita menjalankan jenkin dengan menggunakan docker container maka kita harus masuk ke console interaktif dari container jenkins tersebut
untuk masuk ke dalam interatiive console dari docker ada bisa mengetikann script seperti di bawah ini:

```bash
docker container exec \
[CONTAINER ID or NAME] \
    sh -c "cat /var/jenkins_home/secrets/initialAdminPassword"
```
#### Contoh :
```bash
docker container exec f8a86daae71e sh -c "cat /var/jenkins_home/secrets/initialAdminPassword"
```
setelah password di dapatkan anda tinggal mengikuti step2 intalasi plugin dan jenkins siap untuk di gunakan 

